FROM ruby:2.3-alpine
MAINTAINER milan.santosi@gmail.com

ENV BUILD_PACKAGES="curl-dev ruby-dev build-base bash" \
    DEV_PACKAGES="zlib-dev libxml2-dev libxslt-dev tzdata yaml-dev sqlite-dev" \
    RUBY_PACKAGES="ruby-json yaml nodejs"

RUN apk add --no-cache $BUILD_PACKAGES $DEV_PACKAGES $RUBY_PACKAGES

WORKDIR /helloworld
COPY . /helloworld
RUN bundle config build.nokogiri --use-system-libraries && \
    bundle install && \
    bundle clean

EXPOSE 3000

ENTRYPOINT ["rails", "server", "-b", "0.0.0.0"]
