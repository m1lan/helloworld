# Hello, World! #

Everyone's favorite demo app ;)

Running here: http://default-environment.nujynpvsm3.eu-central-1.elasticbeanstalk.com/

Also: an example of a tiny Alpine Linux image with minimal RoR
dependencies which can be used as a template for multi-container
deployment scenarios.

## Docker ##

Prerequisites: You need to have Docker installed, get it here
https://www.docker.com or use a VM running Linux or the 'Docker
Toolbox' if you're using Windows or OSX. Docker is currently 64-bit
only.


```
#!bash

docker build -t alpinehelloworld .

docker run --name helloworld -p 8080:3000 -d alpinehelloworld
```
Then, visit http://localhost:8080/

Again: Docker is first and foremost a Linux software, it will probably
not run well on Microsoft Windows or Apple OSX. This is mostly because
of the added overhead of an underlying VM that needs to communicate
with the host system, might be you'll also need to adjust your NAT or
adapter-bridging setting.  I have used Ubuntu 15.10 to run my local
installation and deployed to Amazon EBS and Digital Ocean droplets.


## EBS Deployment with Codeship##

Set up your new EBS Docker application in the Beanstalk console and in
doing so, upload an initial version (described below). Alternatively
use the tutorial wizard, it will install a Sample App (php) which we
can use as a placeholder.  Or, you can do the same with the aws eb cli
from the command line, these steps are taken from the Beanstalk site:

```
#!bash
# Get Started
mkdir HelloWorld
cd HelloWorld
eb init -p PHP
echo "Hello World" > index.html
eb create dev-env
eb open

```


On the Codeship site after you've added your bitbucket repo as a new
Project in Codeship, go to "Project Settings", then to "Configure Your
Deployment Pipelines".  Add AWS Access Key ID, AWS Secret Access Key,
Region, Application Name, Environment Name and S3 Bucket info.

Now you can make changes to the master branche to automatically
trigger the deployment chain.



## EBS Deployment without Codeship##

Uploading the EBS_Dockerfile (runs a git clone) or a .zip file of the
repo will automatically set up the container, and then start the rails
application server.

Alternatively, use the "eb" tool, or deploy through integration with
your favorite CI and/or QA chain.




## Run without Docker ##

To run this locally, without Docker, you will have to install ruby +
rails + dependencies yourself the way it's normally done on your
platform. Then, do 'bundle install' to install the dependencies from
the Gemfile, then 'rails server' to start the app. Congratulations,
you've just turned your local machine into a dependency. :p

## Why should I even bother? ##

Because now, you can finally stop worrying about the platform you are
developing on (or for) and start developing only for Docker as a
platform.

To explain a bit more what this means, I've decided to create two
'Dockerfiles' for two slightly different scenarios,- they differ
marginally and the second example can actually be used in exactly the
same way as the first. For purposes of clarity of this explanation,
I've split them up. In an real use-case, changes like this would be
done in a separate git branch of the same file.

The container is rebuild on the fly each time and the updated source
code needs to get in there somehow, so "EBS_Dockerfile" is an example
of this: there is an added "git clone" to show a very simple
deployment.  The non-EBS Dockerfile only works if it's run from the
root of the repo because it copies the source tree from $PWD into the
container.

So, either way, on the Dev's hard disk, the git controlled source tree
is accessible outside of the container, while all run- and build time
dependencies are managed by the source-controlled Dockerfile.  Because
of that, the developer will never again have to clutter his local
machine with dependencies, while also instantly yielding a minimal
container with everything needed to build, test, deploy and run the
app, with the *only* thing ever needed being a working installation of
Docker, which runs best on Linux or alternatively in a Linux VM or the
"Docker Toolbox". The main idea is to no longer develop in environment
A and then deploy to environment B,- instead, the environment is
abstracted away.

Another side-effect of this design is that we get configuration
management and host provisioning "for free". Naturally, it's also
possible for an app to intelligently modify it's own
host-infrastructure or even spawn different versions of itself (I
know, it's a bit scary).

Naturally, the result can also be exported by hand as a stand alone
image or merged back into master,- everything is nothing but a text
file under version control.

## Note on Security ##

This is just a placeholder app for a deployment chain demo.  It's
based on a plain "rails new".  The generated config/secrets.yml is
included in order not to break the demo and it should never be
uploaded like this in a real scenario.

## TODO ##
- do the same with docker-compose
- deploy 2 containers to multi-container environment and let them interact (example: simple app <-> postgres interaction)